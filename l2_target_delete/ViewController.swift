//
//  ViewController.swift
//  l2_target_delete
//
//  Created by Anatolii on 3/23/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //рисуем квадрат с заданными параметрами
        let xPos = 400
        let yPos = 300
        let wight = 200
        let height = 200
        let step = 20
        let numberOfBoxes = 5
        //нарисуем три квадрата в линию
        drawTargetBox(xPos: xPos,
                      yPos: yPos,
                      wight: wight,
                      height: height,
                      step: step,
                      numberOfBoxes: numberOfBoxes)
    }
    
    func drawBox(xPos: Int,
                 yPos: Int,
                 wight: Int,
                 height: Int,
                 color: UIColor) {
        let box = UIView.init(frame: CGRect(x: xPos, y: yPos, width: wight, height: height))
        box.backgroundColor = color
        box.layer.cornerRadius = CGFloat(wight/2)
        view.addSubview(box)
    }
    
    func drawTargetBox(xPos: Int,
                       yPos: Int,
                       wight: Int,
                       height: Int,
                       step: Int,
                       numberOfBoxes: Int) {
        var currXPos = xPos
        var currYPos = yPos
        var currWight = wight
        var currHeight = height
        var color = UIColor.blue
        for _ in 0..<numberOfBoxes {
            drawBox(xPos: currXPos,
                    yPos: currYPos,
                    wight: currWight,
                    height: currHeight,
                    color: color)
            currXPos+=step
            currYPos+=step
            currWight-=step*2
            currHeight-=step*2
            if color == UIColor.blue {
                color = UIColor.red
            }
            else {
                color = UIColor.blue
            }
        }
    }
}



